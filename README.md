# chat app_django channels 



## Getting started
first install redis

## how to install redis
installation: https://redis.io/docs/getting-started/installation/install-redis-on-linux/

#start from here

sudo apt install lsb-release curl gpg
curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list

sudo apt-get update
sudo apt-get install redis

### or Install from Snapcraft
sudo snap install redis

### important commands for redis:
sudo systemctl start redis-server
sudo systemctl stop redis-server
sudo systemctl restart redis-server
sudo systemctl status redis-server

## create venv and install dependencies
in bash or terminal write below to create venv and install dependencies

### creating chatenv
```
python3 -m venv chatenv
```
after creating environment activate that environment then install dependencies

### installing dependencies

```
pip install -r requirements.txt 
```
## migrate database

### makemigrations
```
python manage.py makemigrations
```
### migrate database
```
python manage.py migrate
```
these commands will create 15 tables in your database

## create superuser
### create superuser with system username

```
python manage.py createsuperuser
```

enter username : system
enter email : press enter to skip
enter password : as_your_choice

## runserver
### must run server with below command and if you want to use other command to run then change chat > view.py > line 23 to 35 accordingly.

### start redis server if it is not already running
sudo systemctl start redis-server

### run server
```
python manage.py runserver
```

above command will use ASGI/Daphne not wsgi server
now goto:

localhost:8000/

## file uploading not work in localip(ex:- 192.168.xx.xx), it only works on localhost
### to solve that add localip in chrome flags
go to insecure origins treated as secure, add localip and reload chrome

## features
this app includes chat, file upload, create group,leave group, edit group info, edit messages, delete messages, invite code for group, kick members, group members status like last seen, typing..., and etc...
