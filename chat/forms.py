from django import forms

class InvitationForm(forms.Form):
    invitation = forms.CharField(label="Invitaion Code", max_length=255)
    
class CreateGroup(forms.Form):
    name = forms.CharField(label="Group Name", max_length=255)
    description = forms.CharField(widget=forms.Textarea(attrs={"rows":"5"}))
    groupimage = forms.ImageField()
    
class fileUploads(forms.Form):
    userFile = forms.FileField()