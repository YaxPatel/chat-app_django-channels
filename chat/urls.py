from django.urls import path
from .views import signup_view, home, chat, Group, fileUp, leaveGroup, kickUser, upload_file_chunk, logout_view
from django.contrib.auth import views as auth_views

app_name = 'chat'
urlpatterns = [
    path('login/', auth_views.LoginView.as_view(), name="login"),
    path('logout/', logout_view, name="logout"),
    path('', home, name="home"),
    path('Group/', Group, name="Group"),
    path('room/<str:room_name>', chat, name="chat_room"),
    path('kickuser/<int:id>', kickUser, name="kickUser"),
    path('leave/', leaveGroup, name="leaveGroup"),
    path('signup/', signup_view, name="signup"),
    path('fileUp/', fileUp, name="fileUp"),
    path('file_upload_chunks/', upload_file_chunk, name="file_upload_chunks"),
]