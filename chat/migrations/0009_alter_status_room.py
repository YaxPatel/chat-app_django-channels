# Generated by Django 4.2.4 on 2023-08-03 06:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0008_status_room'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='room',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='chat.room'),
        ),
    ]
