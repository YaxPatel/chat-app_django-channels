from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from chat.models import Room,Chat, Status
from chat.views import systemUser
import json

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.group_name = f"{self.room_name}_group"
        self.roomobj = await database_sync_to_async(Room.objects.get)(slug = self.room_name)
        await self.channel_layer.group_add(
            self.group_name, self.channel_name
        )
        
        # for authentication
        self.user = self.scope["user"]
        if self.user.is_authenticated:
            userStatus, created = await database_sync_to_async(Status.objects.update_or_create)(user = self.user, defaults={"status": True, "room": self.roomobj})
            # print(userStatus.room.name, created)
            # roomdb = Room(name = "test_room", description = "demo room", created_by=self.scope["user"])
            # await database_sync_to_async(roomdb.save)()
            await self.accept()
            await self.channel_layer.group_send(
                self.group_name,
                {
                    'type': 'status_change',
                    'message': {"status": True},
                    'username': self.scope["user"].username
                }
            )

        else:
            await self.close()

    async def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        messageType = text_data_json['type']
        if (messageType == "chat_message"):
            message = text_data_json['message']
            messagedb = Chat(user = self.scope["user"], message = message, room = self.roomobj)
            await database_sync_to_async(messagedb.save)()
            await self.channel_layer.group_send(
                self.group_name,
                {
                    'type': 'chat_message',
                    'message': {"text":message,"id":messagedb.id},
                    'username': self.scope["user"].username
                }
            )
        
        elif (messageType == "typing"):
            await self.channel_layer.group_send(
                self.group_name,
                {
                    'type': 'typing',
                    'username': self.scope["user"].username
                }
            )
        # self.send(text_data=json.dumps({"status":"data received!"}))
        # return super().receive(text_data, bytes_data)
        elif (messageType == "edit_message"):
            message = text_data_json['message']
            messageId = text_data_json['id']
            messagedb = await database_sync_to_async(Chat.objects.get)(id = messageId, user = self.scope["user"])
            messagedb.message = message
            messagedb.isEdited = True
            await database_sync_to_async(messagedb.save)()
            await self.channel_layer.group_send(
                self.group_name,
                {
                    'type': 'edit_message',
                    'message': {"text":message,"id":messagedb.id},
                    'username': self.scope["user"].username
                }
            )
        
        elif (messageType == "delete_message"):
            messageId = text_data_json['id']
            messagedb = await database_sync_to_async(Chat.objects.get)(id = messageId, user = self.scope["user"])
            await database_sync_to_async(messagedb.delete)()
            await self.channel_layer.group_send(
                self.group_name,
                {
                    'type': 'delete_message',
                    'status': True,
                    'id':messageId,
                    'username': self.scope["user"].username
                }
            )

        elif (messageType == "chat_file_pending"):
            await self.channel_layer.group_send(
                self.group_name,
                {
                    'type': 'chat_file_pending',
                    'username': self.scope["user"].username
                }
            )
        
        elif (messageType == "edit_group"):
            message = text_data_json['message']
            roomobj = await database_sync_to_async(Room.objects.get)(slug = self.room_name, created_by = self.user)
            roomobj.name = message.get("name")
            roomobj.description = message.get("description")
            await database_sync_to_async(roomobj.save)()
            messagedb = Chat(user = systemUser, message = f"Group info changed by {self.user}", room = self.roomobj)
            await database_sync_to_async(messagedb.save)()
            await self.channel_layer.group_send(
                self.group_name,
                {
                    "type":"system_message", 
                    "message":{"text": f"Group info changed by {self.user.username}", "type": "edit_group"}, 
                    "username": self.user.username,
                    "groupName": message.get("name"),
                    "groupDescription": message.get("description"),
                }
            )
            
            
    
    async def disconnect(self, code):
        userStatus, created = await database_sync_to_async(Status.objects.update_or_create)(user = self.user, defaults={"status": False})
        await self.channel_layer.group_discard(
            self.group_name, self.channel_name
        )
        await self.channel_layer.group_send(
                self.group_name,
                {
                    'type': 'status_change',
                    'message': {"status": False, "last_seen":str(userStatus.last_seen)},
                    'username': self.scope["user"].username
                }
            )
        await self.close()
        return super().disconnect(code)
    
    async def chat_message(self, event):
        await self.send(text_data=json.dumps(event))
        
    async def typing(self, event):
        await self.send(text_data=json.dumps(event))
        
    async def edit_message(self, event):
        await self.send(text_data=json.dumps(event))
        
    async def delete_message(self, event):
        await self.send(text_data=json.dumps(event))
    
    async def chat_file_completed (self, event):
        await self.send(text_data=json.dumps(event))
    
    async def chat_file_pending (self, event):
        await self.send(text_data=json.dumps(event))
    
    async def system_message(self, event):
        await self.send(text_data=json.dumps(event))
    
    async def status_change(self, event):
        await self.send(text_data=json.dumps(event))

# class ChatConsumer(AsyncWebsocketConsumer):
#     async def connect(self):
#         self.room_name = self.scope['url_route']['kwargs']['room_name']
#         self.room_group_name = 'chat_%s' % self.room_name

#         # Join room group
#         await self.channel_layer.group_add(
#             self.room_group_name,
#             self.channel_name
#         )

#         await self.accept()

#     async def disconnect(self, close_code):
#         # Leave room group
#         await self.channel_layer.group_discard(
#             self.room_group_name,
#             self.channel_name
#         )

#     # Receive message from WebSocket
#     async def receive(self, text_data):
#         text_data_json = json.loads(text_data)
#         message = text_data_json['message']

#         # Send message to room group
#         await self.channel_layer.group_send(
#             self.room_group_name,
#             {
#                 'type': 'chat_message',
#                 'message': message
#             }
#         )

#     # Receive message from room group
#     async def chat_message(self, event):
#         message = event['message']

#         # Send message to WebSocket
#         await self.send(text_data=json.dumps({
#             'message': message
#         }))