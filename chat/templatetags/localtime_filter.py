from django import template
from django.utils import timezone

register = template.Library()

@register.filter(name='localtime')
def localtime_filter(value):
    if value is None:
        return ''

    # Convert the datetime value to the current timezone of the server
    local_time = timezone.localtime(value)
    # Return the local time in the desired format
    return local_time.strftime('%Y-%m-%d %H:%M:%S %Z')