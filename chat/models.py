from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
import random
import string
import os
from django.dispatch import receiver
# Create your models here.

class Room (models.Model):
    name = models.CharField("Room Name", null= False, blank=False, max_length=255)
    description = models.TextField("Description", null=False, blank=False)
    image = models.ImageField("Image", upload_to="images")
    invite = models.CharField("Invite Link", max_length=255)
    slug = models.SlugField(unique=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField("created at", auto_now_add=True, blank=True)
    updated_at = models.DateTimeField("updated at", auto_now=True, blank=True)
    
    def save(self, *args, **kwargs):
        self.invite = ''.join(random.choices(string.ascii_uppercase +
                             string.digits, k=10))
        if not self.invite:
            qs_exists = Room.objects.filter(invite=self.invite).exists()
            while(qs_exists):
                if qs_exists:
                    new_invite = ''.join(random.choices(string.ascii_uppercase +
                             string.digits, k=10))
                self.invite = new_invite
                qs_exists = Room.objects.filter(invite=self.invite).exists()
                
        if not self.slug:
            self.slug = slugify(self.name)
            qs_exists = Room.objects.filter(slug=self.slug).exists()
            while(qs_exists):
                if qs_exists:
                    new_slug = "{slug}-{randstr}".format(
                        slug=self.slug,
                        randstr=random.randint(0, 10000)
                    )
                self.slug = slugify(new_slug)
                qs_exists = Room.objects.filter(slug=self.slug).exists()
        super(Room, self).save(*args, **kwargs)

class Chat (models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.TextField("Message", null=True, blank=True)
    file = models.FileField(upload_to='files', null=True, blank=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    isEdited = models.BooleanField("Is Edited ?",default=False)
    created_at = models.DateTimeField("created at", auto_now_add=True, blank=True)
    updated_at = models.DateTimeField("updated at", auto_now=True, blank=True)

@receiver(models.signals.post_delete, sender=Chat)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)
    
class UsersPerRoom(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    created_at = models.DateTimeField("created at", auto_now_add=True, blank=True)
    updated_at = models.DateTimeField("updated at", auto_now=True, blank=True)

class Status(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.SET_NULL, null=True)
    status = models.BooleanField("Stauts", default=False)
    last_seen = models.DateTimeField("updated at", auto_now=True, blank=True)