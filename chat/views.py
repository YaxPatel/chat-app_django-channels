from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from chat.models import Chat, Room, UsersPerRoom, Status, User
from .forms import InvitationForm, CreateGroup, fileUploads
from django.contrib import messages
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
import os
import shutil
from django.core.files import File
from .templatetags.filename import filename as FileName
from django.contrib.auth import logout
import os
import signal
from django.db.utils import OperationalError
import sys

if 'runserver' in sys.argv:
    try:
        systemUser = User.objects.get(username = "system", is_superuser = True)
    except User.DoesNotExist:
        # Print message in red color
        print("\033[91mcreate superuser named system then runserver.\033[0m")
        # Send Ctrl+C signal
        os.kill(os.getpid(), signal.SIGINT)
    except OperationalError as e:
        print("\033[91mfirst Migrate all table and create superuser named system then runserver.\033[0m")
        os.kill(os.getpid(), signal.SIGINT)

   
channel_layer = get_channel_layer()

def signup_view(request):
    form = UserCreationForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy('chat:login'))
    return render(request,'signup.html',{'form':form})

@login_required
def home(request):
    form = InvitationForm(request.POST or None)
    rooms = UsersPerRoom.objects.filter(user = request.user)
    if form.is_valid():
        data = form.cleaned_data
        # OBQBEKL47M
        try:
            roomobj = Room.objects.get(invite = data['invitation'])
            UsersPerRoomData, created = UsersPerRoom.objects.get_or_create(user = request.user, room = roomobj)
            if created:
                messages.success(request, "Join Successfully!", "alert-success")
                message = f"{request.user.username} Join the Group."
                chatobj = Chat(user = systemUser, message = message, room = roomobj)
                chatobj.save()
                async_to_sync(channel_layer.group_send)(f"{roomobj.slug}_group",{"type":"system_message", "message":{"text":message, "type": "join_group"}, "username": request.user.username, "userid": request.user.id})
            else:
                messages.success(request, "You Already In Group! ", "alert-info")
        except Room.DoesNotExist:
            messages.warning(request, "It's Look-like Invitaion expired!", "alert-warning")
        except Exception as e:
            messages.error(request,"Something went Wrong!","alert-danger")
        return redirect(reverse_lazy('chat:home'))
    return render(request, 'home.html', {"chat": chat, "form": form, "rooms": rooms})

@login_required
def chat(request, room_name):
    try:
        roomobj = Room.objects.get(slug = room_name)
    except Room.DoesNotExist:
        messages.error(request, "Room Does Not Exists!", "alert-warning")
        return redirect(reverse_lazy('chat:home'))
    userChk = UsersPerRoom.objects.filter(room = roomobj, user = request.user).exists()
    # Get the users in a particular room
    users_in_room = User.objects.filter(usersperroom__room=roomobj)

    # Get the status of each user in the room
    userStatus = Status.objects.filter(user__in=users_in_room).order_by("-status").exclude(user = request.user)

    if not userChk:
        messages.info(request, "First Join Group using Invitation link.", "alert-primary")
        return redirect(reverse_lazy('chat:home')) 
    chat = Chat.objects.filter(room = roomobj)
    return render(request, 'chat.html', {"chat": chat, "room_name":room_name, "roomobj": roomobj, "userStatus": userStatus})

@login_required
@require_http_methods(["POST"])
def Group(request):
    form = CreateGroup(request.POST, request.FILES)
    if form.is_valid():
        data = form.cleaned_data
        roomobj = Room(name=data.get('name'), description = data.get('description'), image = data.get('groupimage'),created_by=request.user)
        roomobj.save()
        UsersPerRoomData = UsersPerRoom(user = request.user, room= roomobj)
        UsersPerRoomData.save()
        messages.success(request,"Group Created Successfully ^_^","alert-success")
    else:
        print(form.errors)
        messages.error(request,"Something went Wrong!","alert-danger")
    return redirect(reverse_lazy('chat:home'))

@login_required
@require_http_methods(["POST"])
@csrf_exempt
def fileUp(request):
    form = fileUploads(request.POST,request.FILES)
    if form.is_valid():
        data = form.cleaned_data
        print(request.FILES['userFile'].content_type)
        group_name = request.headers['Referer'].split("/")[-1]
        roomobj = Room.objects.get(slug = group_name)
        chatobj = Chat(user = request.user, room = roomobj, file = data['userFile'])
        chatobj.save()
        async_to_sync(channel_layer.group_send)(f"{group_name}_group", {"type": "chat_file_completed", "file_name":f'{request.FILES["userFile"].name}', "file_type" : f'{request.FILES["userFile"].content_type}', "id": chatobj.id, "username":request.user.username, "url":str(chatobj.file)})

    else:
        print(form.errors)
        return JsonResponse({"status":"fail"}, status = 400)
    return JsonResponse({"status":"success"},status=200)

async def upload_file_chunk(request):
    file = request.FILES.get('chunk')
    total_chunks = int(request.POST.get('totalChunks'))
    chunk_index = int(request.POST.get('chunkIndex'))

    # Extract the file ID from the request
    file_id = request.POST.get('fileId')
    print(total_chunks, chunk_index)
    filename = request.POST.get('filename')
    upload_dir = os.path.join('uploads/files', file_id)

    # Create the upload directory if it doesn't exist
    if not os.path.exists(upload_dir):
        os.makedirs(upload_dir)

    # Build the path to the chunk file
    chunk_name = f'{filename}.part{chunk_index}'
    chunk_path = os.path.join(upload_dir, chunk_name)

    # Save the chunk data to a file
    with open(chunk_path, 'wb') as f:
        f.write(file.read())

    # Check if all chunks have been uploaded
    if chunk_index == total_chunks - 1:
        # Combine the chunks into the final file
        final_path = os.path.join(upload_dir, filename)
        with open(final_path, 'wb') as f:
            for i in range(total_chunks):
                chunk_name = f'{filename}.part{i}'
                chunk_path = os.path.join(upload_dir, chunk_name)
                with open(chunk_path, 'rb') as chunk_file:
                    f.write(chunk_file.read())
                
        # Delete the chunk files
        for i in range(total_chunks):
            chunk_name = f'{filename}.part{i}'
            chunk_path = os.path.join(upload_dir, chunk_name)
            os.remove(chunk_path)
        
        with open(final_path, 'rb') as f:
            group_name = request.headers['Referer'].split("/")[-1]
            roomobj = await Room.objects.aget(slug = group_name)
            chatobj = await Chat.objects.acreate(user = request.user, room = roomobj, file = File(f, name=filename))

        shutil.rmtree(upload_dir)
        await channel_layer.group_send(f"{group_name}_group", {"type": "chat_file_completed", "file_name":f'{FileName(chatobj.file)}', "id": chatobj.id, "username":request.user.username, "url":str(chatobj.file)})
        return JsonResponse({'success': True})

    return JsonResponse({'success': False})

# async def upload_file_chunk(request):
#     file = request.FILES.get('chunk')
#     total_chunks = int(request.POST.get('totalChunks'))
#     chunk_index = int(request.POST.get('chunkIndex'))

#     # Extract the file ID from the request
#     file_id = request.POST.get('fileId')
#     print(total_chunks, chunk_index)
#     filename = request.POST.get('filename')
#     upload_dir = os.path.join('uploads/files', file_id)

#     # Create the upload directory if it doesn't exist
#     if not os.path.exists(upload_dir):
#         os.makedirs(upload_dir)

#     # Build the path to the chunk file
#     chunk_name = f'{filename}.part{chunk_index}'
#     chunk_path = os.path.join(upload_dir, chunk_name)

#     # Save the chunk data to a file
#     with open(chunk_path, 'wb') as f:
#         f.write(file.read())

#     # Check if all chunks have been uploaded
#     if chunk_index == total_chunks - 1:
#         # Combine the chunks into the final file
#         final_path = os.path.join(os.path.dirname(upload_dir), filename)
#         with open(final_path, 'wb') as f:
#             for i in range(total_chunks):
#                 chunk_name = f'{filename}.part{i}'
#                 chunk_path = os.path.join(upload_dir, chunk_name)
#                 with open(chunk_path, 'rb') as chunk_file:
#                     f.write(chunk_file.read())

#         # Delete the chunk files
#         # for i in range(total_chunks):
#         #     chunk_name = f'{filename}.part{i}'
#         #     chunk_path = os.path.join(upload_dir, chunk_name)
#         #     os.remove(chunk_path)
#         # os.rmdir(upload_dir)
#         shutil.rmtree(upload_dir)

#         return JsonResponse({'success': True})

#     return JsonResponse({'success': False})

@login_required
def leaveGroup(request):
    user = request.user
    try:
        room_name = request.headers['Referer'].split("/")[-1]
        roomChk = Room.objects.get(slug = room_name)
        userChk = UsersPerRoom.objects.get(user = user, room = roomChk)
        userChk.delete()
        message = f"{user.username} Left the Group."
        chatobj = Chat(user = systemUser, message = message, room = roomChk)
        chatobj.save()
        messages.success(request, f"You Left {room_name} Group Successfully!", "alert-success")
        async_to_sync(channel_layer.group_send)(f"{room_name}_group",{"type":"system_message", "message":{"text":message, "type": "leave_group"}, "username": user.username})
    except Room.DoesNotExist:
        messages.warning(request, "Room Does Not Exists!", "alert-warning")
    except UsersPerRoom.DoesNotExist:
        messages.warning(request, "You Already Left Group!", "alert-warning")
    except Exception as e:
        messages.error(request,"Something went Wrong!","alert-danger")
    return redirect(reverse_lazy('chat:home'))
    
@login_required
def kickUser(request, id):
    try:
        room_name = request.headers['Referer'].split("/")[-1]
        userobj = User.objects.get(id = id)
        roomobj = Room.objects.get(slug = room_name, created_by = request.user)
        UsersPerRoomObj = UsersPerRoom.objects.get(user = userobj, room = roomobj)
        UsersPerRoomObj.delete()
        message = f"{userobj.username} kicked by {request.user.username}"
        chatobj = Chat.objects.create(user = systemUser, message = message, room = roomobj)
        async_to_sync(channel_layer.group_send)(f"{room_name}_group",{"type":"system_message", "message":{"text":message, "type": "kick_user"}, "username": userobj.username})
        return JsonResponse({"status" : "ok"},status=200)
    except UsersPerRoom.DoesNotExist:
        async_to_sync(channel_layer.group_send)(f"{room_name}_group",{"type":"system_message", "message":{"text":"User Not Exist"}})
        return JsonResponse({"status" : "Bad Request"},status=400)
    except Room.DoesNotExist:
        async_to_sync(channel_layer.group_send)(f"{room_name}_group",{"type":"system_message", "message":{"text":"Room Not Exist"}})
        return JsonResponse({"status" : "Bad Request"},status=400)
    except Exception:
        messages.error(request,"Something went Wrong!","alert-danger")
        return redirect(reverse_lazy('chat:home'))

def logout_view(request):
    logout(request)
    return redirect(reverse_lazy('chat:login'))